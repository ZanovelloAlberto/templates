#include "sublib1.hpp"
#include "sublib2.hpp"

int main(int argc, char *argv[])
{
    sublib1 hi;
    hi.print();

    sublib2 howdy;
    howdy.print();
    
    return 0;
}
