# Set the project name
project (sublibrary1)

SET(headers
    include/sublib1.hpp
	# ${BulletInverseDynamicsDetails_HDRS}
)


# Add a library with the above sources
add_library(${PROJECT_NAME} src/sublib1.cpp  ${headers})
add_library(sub::lib1 ALIAS ${PROJECT_NAME})


set_target_properties(${PROJECT_NAME} PROPERTIES PUBLIC_HEADER ${headers})

target_include_directories( ${PROJECT_NAME}
    PUBLIC ${PROJECT_SOURCE_DIR}/include
)

INSTALL(TARGETS ${PROJECT_NAME} 
    LIBRARY DESTINATION lib
    PUBLIC_HEADER DESTINATION include

)
