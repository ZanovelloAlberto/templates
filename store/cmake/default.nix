let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
in
{ pkgs ? import <nixpkgs> { } }:
pkgs.clangStdenv.mkDerivation rec {
  pname = "sample";
  version = "0.1.1";
  buildInputs = with pkgs;[ cmake ninja gnumake ];
  src = ./.;
  # CMAKE_GENERATOR = "${pkgs.ninja}";
  CMAKE_GENERATOR = "Ninja";
  buildPhase = ''
  '';
  shellHook = ''
    b(){
      mkdir build
      cd build
      cmake ..
      ninja
      cd ..
    }
    c(){
      sudo rm -r build install
    }
    i() {
      mkdir -p install 
      cd build 
      cmake --install . --prefix ../install/
      cd ..
    }
  '';
}
