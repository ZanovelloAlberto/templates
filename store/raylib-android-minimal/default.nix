let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
in
{ pkgs ? import <nixpkgs> { android_sdk.accept_license = true; } }:
let
  myand = pkgs.androidenv.composeAndroidPackages rec {
    # platformToolsVersion = "30.0.5";
    # buildToolsVersions = [ "30.0.3" ];
    includeNDK = true;
    useGoogleAPIs = false;
    useGoogleTVAddOns = false;
    includeSources = false;
    includeSystemImages = false;
    includeEmulator = false;
    # emulatorVersion = "30.3.4";
    platformVersions = [ "26" ];
    abiVersions = [ "x86" "x86_64" ];
  };



  # fhs = pkgs.buildFHSUserEnv {
  #   name = "android-env";
  #   targetPkgs = pkgs: with pkgs;
  #     [
  #       git
  #       gnupg
  #       myand.platform-tools
  #       myand.androidsdk
  #     ];
  #   multiPkgs = pkgs: with pkgs;
  #     [
  #       zlib
  #       ncurses5
  #     ];
  #   runScript = "bash";
  #   profile = ''
  #     export ALLOW_NINJA_ENV=true
  #     export USE_CCACHE=1
  #     export ANDROID_JAVA_HOME=${pkgs.jdk.home}
  #     export LD_LIBRARY_PATH=/usr/lib:/usr/lib32
  #   '';
  # };

  raylib-and = pkgs.clangStdenv.mkDerivation rec {
    name = "rayliband";
    # nativeBuildInputs = [ fhs ];
    LD_LIBRARY_PATH = "${ASDK}/toolchains/llvm/prebuilt/linux-x86_64/lib";
    NIX_DEBUG = 7;

    src = ./test/raylib;

    nativeBuildInputs = with pkgs;[
      # clang++
      # llvmPackages.libcxxClang
      # llvmPackages.libcxxabi
      # llvmPackages.libcxxabi
      libcxx
    ];
    # pkgs.fetchFromGitHub {
    #   owner = "raysan5";
    #   repo = "raylib"; 
    #   rev = "bb18018f292d3332af2c0b426e26f250893c89ad";
    #   hash = "sha256-PoRCyhIa5bczvSR2+75Pu1817x3T/KthHYSLKijljlI=";
    # };
    buildPhase = ''
      echo ${pkgs.libcxx}
      echo ${pkgs.llvmPackages.libcxxClang}
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${
        with pkgs;
        lib.makeLibraryPath [ libcxx ]
      }"
      echo $LD_LIBRARY_PATH
      ldd ${ANDK}/toolchains/llvm/prebuilt/linux-x86_64/bin/clang
      objdump -p ${ANDK}/toolchains/llvm/prebuilt/linux-x86_64/bin/clang
      ${pkgs.gnumake}/bin/make -C src PLATFORM=PLATFORM_ANDROID ANDROID_NDK=${ANDK} ANDROID_ARCH=arm ANDROID_API_VERSION=29
    '';
    installPhase = '' 

      mkdir -p $out/lib/armeabi-v7a
      mkdir -p $out/include/
      mv src/libraylib.a $out/lib/armeabi-v7a
      cp src/raylib.h $out/include/

      mkdir -p $out/assets
      cp logo/raylib_36x36.png $out/assets/icon_ldpi.png
      cp logo/raylib_48x48.png $out/assets/icon_mdpi.png
      cp logo/raylib_72x72.png $out/assets/icon_hdpi.png
      cp logo/raylib_96x96.png $out/assets/icon_xhdpi.png
    '';
  };

  ASDK = "${myand.androidsdk}/libexec/android-sdk";
  ANDK = "${ASDK}/ndk-bundle";
in

pkgs.clangStdenv.mkDerivation rec {
  inherit ASDK ANDK;
  pname = "sample";
  version = "0.1.1";
  buildInputs = with pkgs;[
    cjson
    meson
    ninja
    pkg-config
    myand.androidsdk
  ];
  src = ./.;

  NATIVE_APP_GLUE_DIR = "${ASDK}/sources/android/native_app_glue";
  # LD_LIBRARY_PATH 
  BUILD_TOOLS = "${ASDK}/build-tools/29.0.3";
  TOOLCHAIN = "${ANDK}/toolchains/llvm/prebuilt/linux-x86_64";
  NATIVE_APP_GLUE = "${ANDK}/sources/android/native_app_glue";

  FLAGS = "-ffunction-sections -funwind-tables -fstack-protector-strong -fPIC -Wall \
	-Wformat -Werror=format-security -no-canonical-prefixes \
	-DANDROID -DPLATFORM_ANDROID -D__ANDROID_API__=29";

  INCLUDES = "-I. -Iinclude -I../include -I${NATIVE_APP_GLUE} -I${TOOLCHAIN}/sysroot/usr/include";


  buildPhase = ''

    # ${ASDK}/keytool -genkeypair -validity 1000 -dname "CN=raylib,O=Android,C=ES" \
    # -keystore raylib.keystore -storepass 'raylib' -keypass 'raylib' \ 
    # -alias projectKey -keyalg RSA
  '';

  installPhase = ''
    mkdir -p $out/bin/
  '';

  shellHook = ''
    echo ${raylib-and}
      echo ${ASDK}
      echo ${ANDK}
      echo ${pkgs.androidenv.androidPkgs_9_0.androidsdk}
  '';
}
