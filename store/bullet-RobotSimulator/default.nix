{ pkgs ? import <nixpkgs> { overlays = [(import ./nix/config.nix)];} }:
pkgs.clangStdenv.mkDerivation rec{
  pname = "sample";
  version = "1.1.1";
  nativeBuildInputs = with pkgs;[
    meson
    ninja
    pkg-config
    cmake
    gnumake
    bullet_13
    eigen
  ];
  src = ./.;

  shellHook = ''
    echo ${pkgs.bullet_13}
    echo ${pkgs.bullet}
      export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${
        with pkgs;
        lib.makeLibraryPath [ libGL xorg.libX11 xorg.libXi ]
      }"

      b() {
        mesonConfigurePhase
        ninjaBuildPhase
        cd .. 
        # find . -mmin -2 -type f -print
        $(find ./build/$1 -mmin -0.1 -type f -print)
      }
      c() {
        rm -r build result
      }
  '';
  installPhase = ''
    mkdir -p $out/bin/
    mv myexe $out/bin/sample
  '';
}
