/*******************************************************************************************
 *
 *   raylib [models] example - loading gltf with animations
 *
 *   LIMITATIONS:
 *     - Only supports 1 armature per file, and skips loading it if there are multiple armatures
 *     - Only supports linear interpolation (default method in Blender when checked
 *       "Always Sample Animations" when exporting a GLTF file)
 *     - Only supports translation/rotation/scale animation channel.path,
 *       weights not considered (i.e. morph targets)
 *
 *   Example originally created with raylib 3.7, last time updated with raylib 4.2
 *
 *   Example licensed under an unmodified zlib/libpng license, which is an OSI-certified,
 *   BSD-like license that allows static linking with closed source software
 *
 *   Copyright (c) 2020-2023 Ramon Santamaria (@raysan5)
 *
 ********************************************************************************************/
#define FLT_MAX 340282346638528859811704183484516925440.0f // Maximum value of a float, from bit pattern 01111111011111111111111111111111

#include "raylib.h"

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1600;
    const int screenHeight = 1000;

    InitWindow(screenWidth, screenHeight, "raylib [models] example - loading gltf");

    // Define the camera to look into our 3d world
    Camera camera = {0};
    camera.position = (Vector3){5.0f, 5.0f, 5.0f}; // Camera position
    camera.target = (Vector3){0.0f, 2.0f, 0.0f};   // Camera looking at point
    camera.up = (Vector3){0.0f, 1.0f, 0.0f};       // Camera up vector (rotation towards target)
    camera.fovy = 45.0f;                           // Camera field-of-view Y
    camera.projection = CAMERA_PERSPECTIVE;        // Camera projection type

    // Load gltf model
    Model model = LoadModel("models/csgo_fbi.glb");

    BoundingBox box = GetMeshBoundingBox(model.meshes[0]); // Get mesh bounding box

    // Load gltf model animations
    // int animsCount = 0;
    // unsigned int animIndex = 0;
    // unsigned int animCurrentFrame = 0;
    // ModelAnimation *modelAnimations = LoadModelAnimations("models/csgo_fbi.glb", &animsCount);

    Vector3 position = {0.0f, 0.0f, 0.0f}; // Set model position

    // DisableCursor();                    // Limit cursor to relative movement inside the window

    SetTargetFPS(60); // Set our game to run at 60 frames-per-second
                      //--------------------------------------------------------------------------------------
    RayCollision collision = {0};
    char *hitObjectName = "None";
    collision.distance = FLT_MAX;
    collision.hit = false;
    Color cursorColor = WHITE;
    // Main game loop
    Ray ray = {0}; // Picking ray

    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        // position.x += 0.1;
        // Update
        //----------------------------------------------------------------------------------
        // UpdateCamera(&camera, CAMERA_THIRD_PERSON);
        // Select current animation
        // if (IsMouseButtonPressed(MOUSE_BUTTON_RIGHT)) animIndex = (animIndex + 1)%animsCount;
        // else if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) animIndex = (animIndex + animsCount - 1)%animsCount;

        // Update model animationy
        // ModelAnimation anim = modelAnimations[animIndex];
        // animCurrentFrame = (animCurrentFrame + 1)%anim.frameCount;
        // UpdateModelAnimation(model, anim, animCurrentFrame);
        //----------------------------------------------------------------------------------
        ray = GetMouseRay(GetMousePosition(), camera);

        RayCollision boxHitInfo = GetRayCollisionBox(ray, box);

        if ((boxHitInfo.hit) && (boxHitInfo.distance < collision.distance))
        {
            collision = boxHitInfo;
            cursorColor = ORANGE;
            hitObjectName = "Box";

            // Check ray collision against model meshes
            RayCollision meshHitInfo = {0};
            for (int m = 0; m < model.meshCount; m++)
            {
                // NOTE: We consider the model.transform for the collision check but
                // it can be checked against any transform Matrix, used when checking against same
                // model drawn multiple times with multiple transforms
                meshHitInfo = GetRayCollisionMesh(ray, model.meshes[m], model.transform);
                if (meshHitInfo.hit)
                {
                    // Save the closest hit mesh
                    if ((!collision.hit) || (collision.distance > meshHitInfo.distance))
                        collision = meshHitInfo;

                    break; // Stop once one mesh collision is detected, the colliding mesh is m
                }
            }

            if (meshHitInfo.hit)
            {
                collision = meshHitInfo;
                cursorColor = ORANGE;
                hitObjectName = "Mesh";
            }
        }
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();

        ClearBackground(RAYWHITE);

        if (boxHitInfo.hit)
            DrawBoundingBox(box, LIME);

        BeginMode3D(camera);

        DrawModel(model, position, 0.1, WHITE); // Draw animated model
        // DrawGrid(10, 1.0f);

        EndMode3D();

        DrawText("Use the LEFT/RIGHT mouse buttons to switch animation", 10, 10, 20, GRAY);
        // DrawText(TextFormat("Animation: %s", anim.name), 10, GetScreenHeight() - 20, 10, DARKGRAY);

        if (collision.hit)
        {
            int ypos = 70;

            DrawText(TextFormat("Distance: %3.2f", collision.distance), 10, ypos, 10, BLACK);

            DrawText(TextFormat("Hit Pos: %3.2f %3.2f %3.2f",
                                collision.point.x,
                                collision.point.y,
                                collision.point.z),
                     10, ypos + 15, 10, BLACK);

            DrawText(TextFormat("Hit Norm: %3.2f %3.2f %3.2f",
                                collision.normal.x,
                                collision.normal.y,
                                collision.normal.z),
                     10, ypos + 30, 10, BLACK);

            // if (triHitInfo.hit && TextIsEqual(hitObjectName, "Triangle"))
                // DrawText(TextFormat("Barycenter: %3.2f %3.2f %3.2f", bary.x, bary.y, bary.z), 10, ypos + 45, 10, BLACK);
        }

        DrawText("Right click mouse to toggle camera controls", 10, 430, 10, GRAY);

        DrawText("(c) Turret 3D model by Alberto Cano", screenWidth - 200, screenHeight - 20, 10, GRAY);

        DrawFPS(10, 10);
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadModel(model); // Unload model and meshes/material

    CloseWindow(); // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}