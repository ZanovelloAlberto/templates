let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
in
{ pkgs ? import <nixpkgs> { } }:
pkgs.clangStdenv.mkDerivation {
  pname = "sample";
  version = "0.1.1";
  buildInputs = with pkgs;[ raylib meson ninja pkg-config];
  src = ./.;
  
  # NIX_DEBUG=7;
  installPhase = ''
  mkdir -p $out/bin/
  cp myexe $out/bin/
  '';
  shellHook = ''
    b() {
      mesonConfigurePhase
      ninjaBuildPhase
      cd ..
      ./build/myexe
    }
    c() {
      rm -r build result
    }
  '';
  # buildPhase = ''
  # echo $PWD
  # echo diocane
  # '';
}
