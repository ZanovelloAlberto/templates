let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
in
{ pkgs ? import <nixpkgs> { } }:
pkgs.stdenv.mkDerivation {
  pname = "sample";
  version = "0.1.1";
  nativeBuildInputs = with pkgs;[
    meson
    ninja
  ];
  src = ./.;

  installPhase = ''
    mkdir -p $out/bin/
    mv myexe $out/bin/sample
  '';
}
