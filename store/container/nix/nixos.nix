# /etc/nixos/configuration.nix

{ pkgs ? import <nixpkgs> { } }:
{
  # Configuring the system hostname
  networking.hostName = "my-nixos-machine";

  # Configuring users
  users.users = {
    user = {
      isNormalUser = true;
      uid = 1000;
      home = "/home/user";
    };
  };

  # Configuring services (Enabling SSH)
  services.openssh.enable = true;

  # Adding packages
  environment.systemPackages = with pkgs; [
    neovim
    wget
  ];

  # Configuring the boot loader
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/sda";

  # Define more system configurations as needed...
}
