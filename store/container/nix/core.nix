{
  pkgs ? import <nixpkgs> {},
  # pkgsLinux ? import <nixpkgs> {system = "x86_64-linux";},
}:
   pkgs.dockerTools.buildImage {
    name = "hello-docker";
    # tag = "latest";
    copyToRoot = pkgs.buildEnv {
      name = "image-root";
      pathsToLink = ["/html" "/bin"];
      paths = [pkgs.coreutils ./../app ];
    };
    config = {
      Env = ["PATH=/bin/"];
      Cmd = [
        "${pkgs.bash}/bin/bash"
      ];
    };
  }
