let
  pkgs = import <nixpkgs> { };

  res = pkgs.dockerTools.buildImage
    {
      name = "hello-docker";
      # tag = "latest";
      copyToRoot = pkgs.buildEnv {
        name = "image-root";
        pathsToLink = [ "/html" "/bin" ];
        paths = [ pkgs.coreutils ./app ];
      };
      config = {
        Env = [ "PATH=/bin/" ];
        Cmd = [
          # "${pkgs.php}/bin/php -S localhost:8080 -t /html" 
          "${pkgs.bash}/bin/bash"
        ];
      };
    };
in
pkgs.stdenv.mkDerivation {
  name = "ciao";
  src = ./.;
  buildInputs = with pkgs;[ podman php terraform ];
  shellHook = ''
    echo ${res}
    # php -S localhost:8080 -t app/html &
    podman load < ${res}
    # podman run -d --name=my_container -p 8080:80 nginx:latest"
  '';
}

