/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: proto/samp.proto */

/* Do not generate deprecated warnings for self */
#ifndef PROTOBUF_C__NO_DEPRECATED
#define PROTOBUF_C__NO_DEPRECATED
#endif

#include "proto/samp.pb-c.h"
void   myapp__my_message__init
                     (Myapp__MyMessage         *message)
{
  static const Myapp__MyMessage init_value = MYAPP__MY_MESSAGE__INIT;
  *message = init_value;
}
size_t myapp__my_message__get_packed_size
                     (const Myapp__MyMessage *message)
{
  assert(message->base.descriptor == &myapp__my_message__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t myapp__my_message__pack
                     (const Myapp__MyMessage *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &myapp__my_message__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t myapp__my_message__pack_to_buffer
                     (const Myapp__MyMessage *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &myapp__my_message__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
Myapp__MyMessage *
       myapp__my_message__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (Myapp__MyMessage *)
     protobuf_c_message_unpack (&myapp__my_message__descriptor,
                                allocator, len, data);
}
void   myapp__my_message__free_unpacked
                     (Myapp__MyMessage *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &myapp__my_message__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
static const ProtobufCFieldDescriptor myapp__my_message__field_descriptors[2] =
{
  {
    "name",
    1,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(Myapp__MyMessage, name),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "age",
    2,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_INT32,
    0,   /* quantifier_offset */
    offsetof(Myapp__MyMessage, age),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned myapp__my_message__field_indices_by_name[] = {
  1,   /* field[1] = age */
  0,   /* field[0] = name */
};
static const ProtobufCIntRange myapp__my_message__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 2 }
};
const ProtobufCMessageDescriptor myapp__my_message__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "Myapp.MyMessage",
  "MyMessage",
  "Myapp__MyMessage",
  "Myapp",
  sizeof(Myapp__MyMessage),
  2,
  myapp__my_message__field_descriptors,
  myapp__my_message__field_indices_by_name,
  1,  myapp__my_message__number_ranges,
  (ProtobufCMessageInit) myapp__my_message__init,
  NULL,NULL,NULL    /* reserved[123] */
};
