let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz) { };
in
{ pkgs ? import <nixpkgs> { overlays = (import ./overlays); } }:
pkgs.stdenv.mkDerivation {
  pname = "sample";
  version = "0.1.1";
  buildInputs = with pkgs;[ ];
  src = ./.;
  buildPhase = ''

  '';

  CAPP = "ll";
  shellHook = with pkgs;
  let 
  in
  ''
    echo $LD_LIBRARY_PATH
    echo "${
    lib.wrap_with_lib [libcxx] 
      ''
        echo $LD_LIBRARY_PATH
        echo ${CAPP}
      ''
    }"
  '';
}
