final: prev:
{
  lib = prev.lib // 
  {
      wrap_with_lib = lib: body: ''
        export TMP_LD_LIBRARY_PATH=$LD_LIBRARY_PATH;
        export LD_LIBRARY_PATH="${
          prev.lib.makeLibraryPath lib
        }"
        ${body}
        export LD_LIBRARY_PATH=$TMP_LD_LIBRARY_PATH
      '';  };
  # aa = prev.bash;
}
