#include "raylib.h"
#include "raymath.h"
#include "../screens.h" //
                        // #define PI 3.14
float speed = 0.3;
// static int finishScreen = 0;
#define NULL 0
// Load gltf model animations
unsigned int animsCount = 6;
unsigned int animIndex = 0;
unsigned int animCurrentFrame = 0;

union Direction
{
    struct
    {
        bool up : 1;
        bool down : 1;
        bool left : 1;
        bool right : 1;
    } values;
    char raw;

} typedef Direction;

Direction d = {0};
Model model;
Model field;
ModelAnimation *modelAnimations;
ModelAnimation anim;
static Camera camera = {0};
Model map;
enum
{
    IDLE = 2,
    RUN = 6
} Animations;

void InitGameScreen(void)
{
    camera.position = (Vector3){100, 100.0f, 100.0f}; // Camera position
    camera.target = (Vector3){20.0f, 0.0f, 20.0f};    // Camera looking at point
    camera.up = (Vector3){0.0f, 1.0f, 0.0f};        // Camera up vector (rotation towards target)
    camera.fovy = 45.0f;                            // Camera field-of-view Y
    camera.projection = CAMERA_ORTHOGRAPHIC;

    model = LoadModel("./models/robote.glb");
    field = LoadModel("./models/untitled.glb");
    modelAnimations = LoadModelAnimations("./models/robot.glb", &animsCount);
    anim = modelAnimations[animIndex];

    Image image = LoadImage("./models/cubicmap.png"); // Load cubicmap image (RAM)
    Texture2D cubicmap = LoadTextureFromImage(image); // Convert image to texture to display (VRAM)
    Mesh mesh = GenMeshCubicmap(image, (Vector3){1.0f, 1.0f, 1.0f});
    map = LoadModelFromMesh(mesh);

    Texture2D texture = LoadTexture("./models/cubicmap_atlas.png");
    map.materials[0].maps[MATERIAL_MAP_DIFFUSE].texture = texture;

    UnloadImage(image);
}
void UpdateGameScreen(void)
{

    anim = modelAnimations[IDLE];
    d.raw = 0;
    if (IsKeyDown(KEY_S))
    {
        camera.target.x += speed;
        camera.position.x += speed;
        d.values.down = true;
        model.transform = MatrixRotateXYZ((Vector3){0, PI / 2, 0});
        anim = modelAnimations[RUN];
    }
    if (IsKeyDown(KEY_D))
    {
        d.values.right = true;
        camera.target.z -= speed;
        camera.position.z -= speed;
        model.transform = MatrixRotateXYZ((Vector3){0, PI, 0});
        anim = modelAnimations[RUN];
    }
    if (IsKeyDown(KEY_A))
    {
        d.values.left = true;
        camera.target.z += speed;
        camera.position.z += speed;
        model.transform = MatrixRotateXYZ((Vector3){0, 0, 0});
        anim = modelAnimations[RUN];
    }
    if (IsKeyDown(KEY_W))
    {
        d.values.up = true;
        model.transform = MatrixRotateXYZ((Vector3){0, PI + PI / 2, 0});
        camera.target.x -= speed;
        camera.position.x -= speed;
        anim = modelAnimations[RUN];
        printf("%f,%f,%f\n", camera.target.x, camera.target.y, camera.target.z);

    }

    animCurrentFrame = (animCurrentFrame + 1) % anim.frameCount;
    UpdateModelAnimation(model, anim, animCurrentFrame);
}
void DrawGameScreen(void)
{
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), LIGHTGRAY);

    // GuiSlider((Rectangle){30, 120, 120, 24}, "z:", NULL, &camera.target.z, -10, +10);
    // GuiSlider((Rectangle){30, 120 + 40, 120, 24}, "px:", NULL, &camera.position.x, -10, +10);
    // GuiSlider((Rectangle){30, 120 + 80, 120, 24}, "py:", NULL, &camera.position.y, -10, +10);
    // GuiSlider((Rectangle){30, 120 + 120, 120, 24}, "pz:", NULL, &camera.position.z, -10, +10);
    // GuiSlider((Rectangle){30, 120 + 120 + 40, 120, 24}, "ux:", NULL, &camera.up.x, -10, +10);
    // GuiSlider((Rectangle){30, 120 + 120 + 80, 120, 24}, "uy:", NULL, &camera.up.y, -10, +10);
    // GuiSlider((Rectangle){30, 120 + 120 + 120, 120, 24}, "uz:", NULL, &camera.up.z, -10, +10);
    BeginMode3D(camera);

    DrawModel(model, camera.target, 1.0f, WHITE); // Draw animated model
    DrawModel(field, (Vector3){0, 0, 0}, 1.0f, WHITE); // Draw animated model
    DrawModel(map, (Vector3){0, 0, 0}, 5.0f, WHITE); // Draw animated model
    // DrawGrid(20, 1.0f);

    EndMode3D();

    // GuiSlider((Rectangle){30, 40, 120, 24}, "x:", NULL, &(camera.position.x), 100, +200);
    // GuiSlider((Rectangle){30, 80, 120, 24}, "y:", NULL, &(camera.position.y), 100, +200);
}
void UnloadGameScreen(void)
{
    UnloadModel(model); // Unload model and meshes/material
    UnloadModel(field); // Unload model and meshes/material
    UnloadModel(map); // Unload model and meshes/material
}
GameScreen FinishGameScreen(void)
{
    return NONE;
    // return finishScreen;
}