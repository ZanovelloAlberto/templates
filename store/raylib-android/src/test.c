#include <stdio.h>
#include "map/map.h"

int main(int argc, char const *argv[])
{
    /* code */
    Object o = {0};
    o.position = (Vector3){1, 1, 1};
    o.item = RED_CUBE;
    cJSON *r = serializeObj(o);

    char *filename = "test.txt";

    // open the file for writing
    FILE *fp = fopen(filename, "w");
    if (fp == NULL)
    {
        printf("Error opening the file %s", filename);
        return -1;
    }

    char *string = cJSON_Print(r);
    if (string == NULL)
    {
        fprintf(stderr, "Failed to print monitor.\n");
    }
    fprintf(fp, string);

    cJSON_Delete(r);
    fclose(fp);

    return 0;
}
