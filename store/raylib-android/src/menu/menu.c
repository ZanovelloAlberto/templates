
#include "raylib.h"
#include "../screens.h"
#define RAYGUI_IMPLEMENTATION
#include "../raygui.h"

static int sg  = 0;
static int cm = 0;
static int quit = 0;

void InitMenuScreen(void)
{
    EnableCursor();
}
void UpdateMenuScreen(void)
{
}
void DrawMenuScreen(void)
{
    ClearBackground(GetColor(GuiGetStyle(DEFAULT, BACKGROUND_COLOR)));
    // printf("diocane");

    // raygui: controls drawing
    //----------------------------------------------------------------------------------
    sg = GuiButton((Rectangle){72, 176, 120, 24}, "START GAME");
    cm = GuiButton((Rectangle){72, 264, 120, 24}, "CREATE MAP");
    quit = GuiButton((Rectangle){72, 344, 120, 24}, "QUIT");
    // GuiButton((Rectangle){72, 50, 120, 24}, "BO");
    //----------------------------------------------------------------------------------
}
void UnloadMenuScreen(void)
{
}
GameScreen FinishMenuScreen(void)
{
    if (sg)
        return GAME;
    if (cm)
        return MAP;
    if (quit)
        return QUIT;
    
    return NONE;
}
