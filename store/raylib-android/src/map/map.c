// static int finishScreen = 0;
#include "map.h"
#include <stdio.h>
#include "../raygui.h"

const char *lnames[3] = {
    "none",
    "red cube",
    "green cube",
};

static tll(Object) objs = tll_init();
static Camera camera = {0};
char *carret = "a;b;c;d;e;f;g;i;h;i;l;m;n;o";
static const unsigned int carretLenght = 18;
static unsigned int listSelected = 0;
static unsigned int listHover = 0;
static int wheelPos = 0;
static bool orth = 0;
// int sliderx = 0;

void InitMapScreen(void)
{
    Object bb = {.position = (Vector3){1, 3, 1}, .item = RED_CUBE};
    Object aa = {.position = (Vector3){5, 1, 0}, .item = RED_CUBE};
    tll_push_front(objs, bb);
    tll_push_front(objs, aa);
    // printf("come cazzo");
    // printf("come cazzo%s",lnames[ITEMS_NONE]);
    // carretLenght = strlen(carret);
    EnableCursor();
    // camera.position = (Vector3){0, 10, 10};      // Camera position
    // camera.target = (Vector3){0.0f, 0.0f, 0.0f}; // Camera looking at point
    // camera.up = (Vector3){0.0f, 1.0f, 0.0f};     // Camera up vector (rotation towards target)
    // camera.fovy = 30.0f;                         // Camera field-of-view Y
    // camera.projection = CAMERA_PERSPECTIVE;

    camera.position = (Vector3){0.0f, 2.0f, -100.0f};
    camera.target = (Vector3){0.0f, 2.0f, 0.0f};
    camera.up = (Vector3){0.0f, 1.0f, 0.0f};
    camera.projection = CAMERA_ORTHOGRAPHIC;
    camera.fovy = 20.0f; // near plane width
}
void drawCarret()
{
}

void UpdateMapScreen(void)
{
    if (orth)
    {
        camera.projection = CAMERA_ORTHOGRAPHIC;
    }
    else
    {
        // camera.
        camera.projection = CAMERA_PERSPECTIVE;
    }

    camera.fovy -= (GetMouseWheelMove() * 2);
    if (camera.fovy < 4)
        camera.fovy = 4;
}
void DrawMapScreen(void)
{

    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), WHITE);

    // DrawTextEx
    BeginMode3D(camera);

    // DrawModel(model, camera.target, 1.0f, WHITE); // Draw animated model
    DrawGrid(30, 1.0f);
    Ray ray = GetMouseRay(GetMousePosition(), camera);
    // GetRayCollisionQuad(ray,)
    // DrawRay
    // ray.position = ray.position + (ray.direction * 3);
    const int scale = 3;
    switch ((listSelected))
    {
    case GREEN_CUBE:
        // DrawCube(ray. position, 1, 1, 1, GREEN);
        DrawSphere((Vector3){ray.position.x + ray.direction.x * scale, ray.position.y + ray.direction.y * scale, ray.position.z + ray.direction.z * scale}, 2, GREEN);
        break;

    case RED_CUBE:
        DrawCube((Vector3){ray.position.x + ray.direction.x * scale, ray.position.y + ray.direction.y * scale, ray.position.z + ray.direction.z * scale}, 1, 1, 1, RED);
        break;

    default:
        DrawRay(ray, RED);
        break;
    }

    tll_foreach(objs, it)
    {
        DrawCube(it->item.position, 1.0, 1.0, 1.0, RED);
    }

    EndMode3D();
    char str[80];
    sprintf(str, "%s;%s;%s", lnames[ITEMS_NONE], lnames[RED_CUBE], lnames[GREEN_CUBE]);
    GuiListView((Rectangle){GetScreenWidth() - 100, 0, 100, GetScreenHeight()}, str, &listHover, &listSelected);
    GuiSlider((Rectangle){30, 40, 120, 24}, "x:", NULL, &camera.target.x, -10, +10);
    GuiSlider((Rectangle){30, 80, 120, 24}, "y:", NULL, &camera.target.y, -10, +10);
    GuiSlider((Rectangle){30, 120, 120, 24}, "z:", NULL, &camera.target.z, -10, +10);
    GuiSlider((Rectangle){30, 120 + 40, 120, 24}, "px:", NULL, &camera.position.x, -10, +10);
    GuiSlider((Rectangle){30, 120 + 80, 120, 24}, "py:", NULL, &camera.position.y, -10, +10);
    GuiSlider((Rectangle){30, 120 + 120, 120, 24}, "pz:", NULL, &camera.position.z, -200, +200);
    GuiSlider((Rectangle){30, 120 + 120 + 40, 120, 24}, "ux:", NULL, &camera.up.x, -10, +10);
    GuiSlider((Rectangle){30, 120 + 120 + 80, 120, 24}, "uy:", NULL, &camera.up.y, -10, +10);
    GuiSlider((Rectangle){30, 120 + 120 + 120, 120, 24}, "uz:", NULL, &camera.up.z, -10, +10);
    GuiCheckBox((Rectangle){30, 120 + 120 + 120 + 40, 120, 24}, "ORTO:", &orth);
    // GuiListView((Rectangle){ 640, 288, 120, 72 }, carret, &ListView000ScrollIndex, &ListView000Active);

    sprintf(str, "whell Pos %f", camera.fovy);
    // const spri
    DrawText(str, 10, 10, 20, Fade(BLACK, 1));
}
void UnloadMapScreen(void)
{
}
GameScreen FinishMapScreen(void)
{
    return NONE;
}