#ifndef MAP_H
#define MAP_H

#include "cJSON.h"
#include "../screens.h"
#include "raylib.h"
#include "tllist.h"

typedef enum {
    ITEMS_NONE = 0,
    RED_CUBE = 1,
    GREEN_CUBE,
} ITEMS;
extern const  char *lnames[3];
typedef struct
{
    ITEMS item;
    Vector3 position;
} Object;

typedef struct
{
    tll(Object) objects;
    // unsigned int objectsNumber;
} mapEditFormat;

cJSON *serializeObj(Object obj);
cJSON *serializeMap(mapEditFormat map);
cJSON *serializeVec(Vector3 vec);

// #else
// #define NORETURN
#endif  