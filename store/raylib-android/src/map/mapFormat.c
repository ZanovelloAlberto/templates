
#include "map.h"

cJSON *serializeMap(mapEditFormat map)
{
    cJSON *o = cJSON_CreateObject();
    cJSON *oa = cJSON_AddArrayToObject(o, "lupara");

    tll_foreach(map.objects, it)
    {
        // cJSON_AddItemToArray(oa, serializeObj(it));

        // printf("forward: %d\n", it->item);
    }
}

cJSON *serializeObj(Object obj)
{
    cJSON *o = cJSON_CreateObject();
    cJSON_AddNumberToObject(o, "objectsNumber", obj.item);
     cJSON_AddItemToObject(o, "objects", serializeVec(obj.position));
}

cJSON *serializeVec(Vector3 vec)
{
    cJSON *ret = cJSON_CreateArray();
    cJSON_AddItemToArray(ret, cJSON_CreateNumber(vec.x));
    cJSON_AddItemToArray(ret, cJSON_CreateNumber(vec.y));
    cJSON_AddItemToArray(ret, cJSON_CreateNumber(vec.z));
    return ret;
}
