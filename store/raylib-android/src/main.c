#include "raylib.h"
#include "raymath.h" // Required for: MatrixRotateXYZ()
#include "screens.h"
GameScreen currentScreen = LOGO;

//------------------------------------------------------------------------------------
// Program main entry point
//------------------------------------------------------------------------------------
int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int screenWidth = 1600;
    const int screenHeight = 1000;
    ChangeToScreen(LOGO);
    InitWindow(screenWidth, screenHeight, "raylib [models] example - loading gltf");

    // Define the camera to look into our 3d world

    // Limit cursor to relative movement inside the window

    SetTargetFPS(60); // Set our game to run at 60 frames-per-second
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose()) // Detect window close button or ESC key
    {
        //----------------------------------------------------------------------------------
        GameScreen moveTo = NONE;
        switch (currentScreen)
        {
        case LOGO:
            UpdateLogoScreen();
            moveTo = FinishLogoScreen();
            break;
        case MENU:
            UpdateMenuScreen();
            moveTo = FinishMenuScreen();
            break;
        case MAP:
            UpdateMapScreen();
            moveTo = FinishMapScreen();
            break;
        case GAME:
            UpdateGameScreen();
            moveTo = FinishGameScreen();
            break;

        default:
            break;
        }
        if (moveTo != NONE)
        {
            ChangeToScreen(moveTo);
        }

        BeginDrawing();

        ClearBackground(RAYWHITE);
        switch (currentScreen)
        {
        case LOGO:
            DrawLogoScreen();
            break;
        case MENU:
            DrawMenuScreen();
            break;
        case MAP:
            DrawMapScreen();
            break;
        case GAME:
            DrawGameScreen();
            break;

        default:
            break;
        }

        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------

    CloseWindow(); // Close window and OpenGL context
    //--------------------------------------------------------------------------------------

    return 0;
}
void ChangeToScreen(GameScreen screen)
{
    switch (currentScreen)
    {
    case LOGO:
        UnloadLogoScreen();
        break;
    case MENU:
        UnloadMenuScreen();
        break;
    case GAME:
        UnloadGameScreen();
        break;
    case MAP:
        UnloadMapScreen();
        break;

    default:
        break;
    }
    switch (screen)
    {
    case LOGO:
        InitLogoScreen();
        break;
    case MENU:
        InitMenuScreen();
        break;
    case GAME:
        printf("\nciao\n");
        InitGameScreen();
        break;
    case MAP:
        InitMapScreen();
        break;

    default:
        break;
    }

    currentScreen = screen;
}
