/**********************************************************************************************
 *
 *   raylib - Advance Game template
 *
 *   Logo Screen Functions Definitions (Init, Update, Draw, Unload)
 *
 *   Copyright (c) 2014 Ramon Santamaria (@raysan5)
 *
 *   This software is provided "as-is", without any express or implied warranty. In no event
 *   will the authors be held liable for any damages arising from the use of this software.
 *
 *   Permission is granted to anyone to use this software for any purpose, including commercial
 *   applications, and to alter it and redistribute it freely, subject to the following restrictions:
 *
 *     1. The origin of this software must not be misrepresented; you must not claim that you
 *     wrote the original software. If you use this software in a product, an acknowledgment
 *     in the product documentation would be appreciated but is not required.
 *
 *     2. Altered source versions must be plainly marked as such, and must not be misrepresented
 *     as being the original software.
 *
 *     3. This notice may not be removed or altered from any source distribution.
 *
 **********************************************************************************************/

#include "raylib.h"
#include "../screens.h"

//----------------------------------------------------------------------------------
// Module Variables Definition (local)
//----------------------------------------------------------------------------------
static int framesCounter = 0;
static int finishScreen = NONE;

static Texture2D title = {0};
static float titleAlpha = 0.0f;

//----------------------------------------------------------------------------------
// Logo Screen Functions Definition
//----------------------------------------------------------------------------------

// Logo Screen Initialization logic
void InitLogoScreen(void)
{
    // Initialize TITLE screen variables here!
    framesCounter = 0;
    
    // title = LoadTexture("./models/raylib_1024x1024.png");
    printf("ha funzionato?\n");
}

// Logo Screen Update logic
void UpdateLogoScreen(void)
{
    // Update TITLE screen variables here!
    framesCounter++;

    titleAlpha += 0.005f;

    if (titleAlpha >= 1.0f)
        titleAlpha = 1.0f;

    // Press enter to change to ATTIC screen
    if ((IsKeyPressed(KEY_ENTER)) || (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)))
    {
        finishScreen = MENU;
    }
}

// Logo Screen Draw logic
void DrawLogoScreen(void)
{
    DrawRectangle(0, 0, GetScreenWidth(), GetScreenHeight(), DARKGRAY);
    DrawText("Loading", 100, 100, 90, Fade(BLACK, titleAlpha));
    // DrawTexture(title, GetScreenWidth() / 2 - title.width / 2, 20, Fade(WHITE, titleAlpha));

    if ((framesCounter > 180) && ((framesCounter / 40) % 2))
        DrawText("PRESS ENTER to START", 380, 545, 40, BLACK);
}

// Logo Screen Unload logic
void UnloadLogoScreen(void)
{
    // Unload TITLE screen variables here!
    UnloadTexture(title);
}

// Logo Screen should finish?
GameScreen FinishLogoScreen(void)
{
    return finishScreen;
}